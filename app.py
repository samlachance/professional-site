from flask import Flask, render_template, abort
from flask.ext.scss import Scss


app = Flask(__name__)
app.debug = True

Scss(app, static_dir='static/styles', asset_dir='static/styles/scss')

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/projects')
def projects():
  return render_template('projects.html')

@app.route('/experience')
def experience():
  return render_template('experience.html')

@app.route('/contact')
def contact():
  return render_template('contact.html')

@app.route('/blog')
def blog():
  return render_template('blog.html')

@app.route('/blog/<id>')
def single_blog(id):
  return render_template('blog.html', post_id=id)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('lost.html'), 404

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=8080)