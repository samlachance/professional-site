FROM alpine:3.6

ENV FLASK_APP=app.py

RUN apk add --update \
    python2 \
    python-dev \
    py-pip \
    build-base \
    bash
RUN pip install virtualenv flask Flask-scss Flask-Reggie
RUN rm -rf /var/cache/apk/*

WORKDIR /app

COPY . /app
RUN virtualenv /env && /env/bin/pip install -r /app/requirements.txt

EXPOSE 8080

ENTRYPOINT ["python"]
CMD ["app.py"]