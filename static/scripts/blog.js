var apiPath = "https://blog.samlachance.com/api/posts"

function postConstructor(id, title, body, author, created_at) {
  var date = new Date(created_at)
  return "<div class='post-content' id='post-" + id +"'>" +
            "<h3>" + title + "</h3>" +
            "<p class='post-info'>" + author + " - " + date.toLocaleString("en-US") + "</p>" +
            body +
          "</div>";
}

function postPreview(id, title) {
  return "<li><a href='blog/" + id + "'>" + title + "</a></li>";
}

if( postId ) {
  var post = $.ajax({
    url: apiPath + "/" + postId
  });

  post.done(function(post) {
    $("div#post").empty();
    $("div.posts-container").hide();
    $("div#post").css("marginRight", "0px");
    $(postConstructor(post.id, post.title, post.body, post.author, post.created_at)).appendTo("div#post");
  });

} else {
  var posts = $.ajax({
    url: apiPath,
    type: "GET",
    crossDomain: true,
    dataType: "json"
  });

  posts.done(function(posts) {
    $("div#post").empty();

    var latestPost = posts[0];
    $(postConstructor(latestPost.id, latestPost.title, latestPost.body, latestPost.author, latestPost.created_at)).appendTo("div#post");

    $.each(posts, function(index, p){
      $(postPreview(p.id, p.title)).appendTo("ul#posts-list");
    });
  });
}
