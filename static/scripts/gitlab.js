function projectConstructor(id, name, description, url, lastUpdated) {
  description = (description == null) ? "" : description
  return  "<div class='git-project' id='project-" + id + "'>" +
            "<h4>" + 
              "<a href='" + url + "' target='_blank'>" + name + "</a>" + 
            "</h4>" +
            "<p class='description'>" + description + "</p>" +
          "</div>";
}

function commitConstructor(shortId, longId, author, time, title, projectUrl) {
  commitUrl = projectUrl + "/commit/" + longId;
  return  "<div class='message-container'>" +
            "<div class='icon'><i class='fa fa-code fa-2x' aria-hidden='true'></i></div>" +
            "<div class='message'>" + 
              "<p><b>" + title + "</b></p>" +
              "<p class='commit-author'>by " + author + "</p>" +
            "</div>" +
          "</div>" +
          "<p class='commit-info'>" +
            "<a class='commit-id' href='" + commitUrl + "'>" + shortId + "</a>" +
          "</p>"; 
}

$("div.all-projects").html("<p class='loading-projects'>Loading GitLab projects...</p>");

// Fetch all projects
var projects = $.ajax({
  url: "https://gitlab.com/api/v4/users/1141113/projects?order_by=last_activity_at&visibility=public",
  type: "GET",
  dataType: "json"
});

projects.done(function(projects) {
  // Clear "Loading dialog"
  $("div.all-projects").empty();

  // Build and populate HTML
  $.each(projects, function(index, p){
    $(projectConstructor(p.id, p.name_with_namespace, p.description, p.web_url, p.last_activity_at)).appendTo("div.all-projects");
  });

  // Fetch latest commit
  $.each(projects, function(index, p){

    // Create path using project id
    commitPath = "https://gitlab.com/api/v4/projects/" + p.id + "/repository/commits/master";

    // Fetch latest commit
    var lastCommit = $.ajax({
      url: commitPath,
      type: "GET",
      dataType: "json"
    });

    // Build and populate HTML
    lastCommit.done(function(commit){
      // Heading
      $("<h5>Latest Commit</h5><div class='latest-commit'></div>").appendTo("div#project-" + p.id);
      // Commit conten
      $(commitConstructor(commit.short_id, commit.id, commit.author_name, commit.created_at, commit.title, p.web_url)).appendTo("div#project-" + p.id + " div.latest-commit");
    });
  });
});